---
layout: page
title: Программист на BlockChain проект на базе HyperLedger Fabric
description: Программист на BlockChain проект на базе HyperLedger Fabric
keywords: поиск работы, программист, docker, kubernetes, blockchain, hyperledger, fabric
permalink: /blockchain/hyperledger/
---

# Программист на BlockChain проект на базе HyperLedger Fabric

Здравствуйте!

Стали обращаться потенциальные клиенты по направлению работы с решением от IBM HyperLedger Fabric.

Я изучил материалы и рассмотрел готовые примеры. Есть желание поработать на такого рода проекте, чтобы набраться опыта.

<br/>


Вот пример переделанного мной за индусом приложения с версии hyperledger fabric 1 на версию 2. 
https://github.com/webmakaka/Blockchain-for-Business-with-Hyperledger-Fabric

Помимо самого hyperledger, для разработки нужно знать и уметь программировать на каком-нибудь подходящем стеке. В данном случае был хорошо мне известный js / typescript.  И само приложение работает с использованием Angular, Node.js и Hyperledger Fabric 2.0. 

<br/>

Собственно, применительно к HyperLedger могут быть полезны следующие мои навыки:

- docker / kubernetes
- node.js / javascript / typescript. В меньшей степени java и golang. (Но на них я тоже способен программировать).
- React, Angular, Vue для форнтенд приложения.

Также понимаю, что необходимо уметь писать тесты, ведь пакеты данного стека обновляются каждый рабочий день.

Ранее приходилось делать какие-то базовые решения на Ethereum со SmartContract на Solidity.


Если вам будет нужен разработчик на проект по hyperledger, дайте знать. Я сейчас буду пытаться найти проект, где можно было бы набраться реального опыта. Согласен писать тесты и разбираться в администрировании. Там по администрированию тоже нужно знать много чего. 

<br/>

**Контакты:**

Группа в телеграм: programmist_net

<br/>

эл.почта:

![Marley](/img/a3333333mail.gif "Marley")