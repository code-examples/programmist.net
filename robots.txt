---
layout: null
permalink: robots.txt
---

User-agent: *
Allow: /
Host: https://programmist.net
Sitemap: https://programmist.net/sitemap.xml